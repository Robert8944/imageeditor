import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.swing.*;

//import com.sun.image.codec.jpeg.*;
@SuppressWarnings("unused")
public class ImageCanvas extends JPanel implements MouseListener, MouseMotionListener{
	private static final long serialVersionUID = 1L;
	private BufferedImage img; //the image that appears on this canvas
	private int[][] pix;
	private int[][] nonColoredPix;
	private Stack<BufferedImage> undo, redo;
	private int w, h, m1x=0, m1y=0, m2x=0, m2y=0;
	private boolean mouseClicked, hasBeenSaved;
	private static final int TYPE = BufferedImage.TYPE_INT_ARGB_PRE;
	
	/** ***************** PIXEL FUNCTIONS ****************** **/
	 public static final int A=0, R=1, G=2, B=3;

	 //returns only the red value of the pixel
	 //   EX: pixel = 0x004f2ca5 --> returns 4f
	 public int howRed(int pixel){return (pixel & 0x00FF0000) >> 16;}
	 public int howRedinPix(int pixel){return (pixel & 0x00FF0000);}

	 //returns only the green value of the pixel
	 //   EX: pixel = 0x004f2ca5 --> returns 2c 
	 public int howGreen(int pixel){return (pixel & 0x0000FF00) >> 8;}
	 public int howGreeninPix(int pixel){return (pixel & 0x0000FF00);}
	 
	 //returns only the blue value of the pixel
	 //   EX: pixel = 0x004f2ca5 --> returns a5
	 public int howBlue(int pixel){return pixel & 0x000000FF;}
	 
	 //returns a new pixel with the specified alpha
	 //    red, green, and blue values
	 public int combine(int a, int r, int g, int b){
		 a = Math.max(0,Math.min(a,255));
		 r = Math.max(0,Math.min(r,255));
		 g = Math.max(0,Math.min(g,255));
		 b = Math.max(0,Math.min(b,255));
		 return a<<32 | r<<16 | g<<8 | b;
	 }
	 public int[][] combine(int[][] a, int[][] b){
		 int[][] ans = new int[h][w];
		 for(int i=0; i<h; i++){
				for(int j=0; j<w; j++){
					int red   = this.howRed(  a[i][j]) + this.howRed(  b[i][j]);
					int green = this.howGreen(a[i][j]) + this.howGreen(b[i][j]);
					int blue  = this.howBlue( a[i][j]) + this.howBlue( b[i][j]);
					ans[i][j] = this.combine(0,red,green,blue);
				}
		 }
		 return ans;
	 }
    /** ***************************************************   **/
	public ImageCanvas(){
		super();
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		undo = new Stack<BufferedImage>();
		redo = new Stack<BufferedImage>();
		this.addMouseListener(this);
		this.setBackground(Color.gray);
		this.setPreferredSize(new Dimension(400,400));
		img = new BufferedImage(100,100,TYPE);
		w = img.getWidth();
		h = img.getHeight();
		pix = imgToArray();
		nonColoredPix = imgToArray();
	}
	public BufferedImage getImage(){return img;}
	public void setImage(File file){
		try{ 
			img = ImageIO.read((file));
			MediaTracker mt = new MediaTracker(new Component(){private static final long serialVersionUID = 1L;});
			mt.addImage(img, 0);
			mt.waitForAll();
		}
		catch(Exception ex){ex.printStackTrace();}
		w = img.getWidth();
		h = img.getHeight();
		pix = imgToArray();
		nonColoredPix = imgToArray();
		this.setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
	}
	// *********************Easy pixel manips************************
	public void red(){
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				pix[i][j] = pix[i][j] & 0x00FF0000;
			}
		}
		this.arrayToImg(pix);
	}
	public void green(){
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				pix[i][j] = pix[i][j] & 0x0000FF00;
			}
		}
		this.arrayToImg(pix);
	}
	public void blue(){
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				pix[i][j] = pix[i][j] & 0x000000FF;
			}
		}
		this.arrayToImg(pix);
	}
	public void normal(){
		this.arrayToImg(nonColoredPix);
		pix = nonColoredPix;
		this.arrayToImg(nonColoredPix);
	}
	public void negative(){
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				pix[i][j] = this.combine(0,255-this.howRed(pix[i][j]),255-this.howGreen(pix[i][j]),255-this.howBlue(pix[i][j]));
			}
		}
		this.arrayToImg(pix);
		
	}
	public void mirror(boolean vert){
		int[][] temp = new int[h][w];
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				temp[i][j] = pix[i][j];
			}
		}
		if(vert){
			for(int i=0; i<h; i++){
				for(int j=0; j<w; j++){
					pix[i][(w-1)-j] = temp[i][j];
				}
			}
		}
		else{
			for(int i=0; i<h; i++){
				for(int j=0; j<w; j++){
					pix[(h-1)-i][j] = temp[i][j];
				}
			}
		}
		this.arrayToImg(pix);
		
	}
	// *********************END Easy pixel manips***********************	
	public void blackandwhite() {
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){//Gray = Green * 0.59 + Blue * 0.11 + Red * 0.30;
				int Red = this.howRed(pix[i][j]);
				int Green = this.howGreen(pix[i][j]);
				int Blue = this.howBlue(pix[i][j]);
				Green *= 0.33;
				Blue *= 0.33;
				Red *= 0.33; 
				int Gray = Red + Blue + Green;
				pix[i][j] = this.combine(0,Gray,Gray,Gray);
			}
		}
		this.arrayToImg(pix);
	}
	public void sepia(double d, double e) {
		this.blackandwhite();
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				int Red = (int) (this.howRed(pix[i][j]) + (d*2));
				int Green = (int) (this.howGreen(pix[i][j]) + d);
				int Blue = (int) (this.howBlue(pix[i][j]) - e);
				pix[i][j] = this.combine(0,Red,Green,Blue);
			}
		}
		this.arrayToImg(pix);
		
	}
	public void toBits(int numBits){
		if(numBits == 2){
			this.blackandwhite();
		}
		else if(numBits == 1){
			for(int i=0; i<h; i++){
				for(int j=0; j<w; j++){
					if(pix[i][j]<(-0x7FFFFF)) {
						pix[i][j] = 0;
					
					}
					else {
						pix[i][j] = 0xFFFFFF;
					}
				}
			}
		}
		this.arrayToImg(pix);
	}
	// ********************KERNEL STUFF *********************************
	public void blur(){
		int num = Integer.parseInt(JOptionPane.showInputDialog(this,"Kernel Number?"));
		int[][] temp = pix;
		for(int i=num; i<h-num; i++){
			for(int j=num; j<w-num; j++){
				double red = 0;
				double green = 0;
				double blue = 0;
				for(int l = -num; l<=num; l++){
					for(int t = -num; t<=num; t++){
							red += this.howRed(pix[i+l][j+t])*(1.0/((double) (num*2+1)*(num*2+1)));
							green += this.howGreen(pix[i+l][j+t])*(1.0/((double) (num*2+1)*(num*2+1)));
							blue += this.howBlue(pix[i+l][j+t])*(1.0/((double) (num*2+1)*(num*2+1)));
					}
				}
				temp[i][j] = ((int)red<<16) + ((int)green<<8) + ((int)blue);
			}
		}
		pix = temp;
		nonColoredPix = temp;
		this.arrayToImg(nonColoredPix);
		this.arrayToImg(pix);
	}
	public void sharpen() {
		pix = imgToArray();
		int[][] temp = new int [h][w];
		int[][] mask = new int [h][w];
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				temp[i][j] = pix[i][j];
			}
		}
		this.blur();
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				int[] colors = new int[3];
				colors[0] = this.howRed(temp[i][j]) + (this.howRed(temp[i][j]) - this.howRed(pix[i][j]));//this.howRed(mask[i][j]);
				colors[1] = this.howGreen(temp[i][j]) + (this.howGreen(temp[i][j]) - this.howGreen(pix[i][j]));//this.howGreen(mask[i][j]);
				colors[2] = this.howBlue(temp[i][j]) + (this.howBlue(temp[i][j]) - this.howBlue(pix[i][j]));//this.howBlue(mask[i][j]);
				for(int l = 0; l<colors.length ; l++){
					colors[l] = Math.max(0,Math.min(colors[l],255));
				}
				pix[i][j] = combine(0,colors[0],colors[1],colors[2]);//colors[0]<<16 | colors[1]<<8 | colors[2];
			}
		}
		this.arrayToImg(pix);
	}
	public Color getAverageColor() {
		double rsum = 0, bsum = 0, gsum = 0, num = 0;
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				rsum+=this.howRed(pix[i][j]);
				gsum+=this.howGreen(pix[i][j]);
				bsum+=this.howBlue(pix[i][j]);
				num+=1;
			}
		}
		return (new Color( (int)(rsum/num), (int)(gsum/num), (int)(bsum/num)));
	}
	public void brighten(int bby){
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				int red = this.howRed(pix[i][j]) + bby;
				red = Math.max(0,Math.min(red,255));
				int green = this.howGreen(pix[i][j]) + bby;
				green = Math.max(0,Math.min(green,255));
				int blue = this.howBlue(pix[i][j]) + bby;
				blue = Math.max(0,Math.min(blue,255));
				pix[i][j] = this.combine(0,red,green,blue);
			}
		}
		this.arrayToImg(pix);
	}
	public int[][] applyKernel(int[][] pix2 ,int[][] kernel){
//		if(kernel.length!= kernel[0].length)
//			return pix2;
//		if(kernel.length%2!=0)
//			return pix2;
		
		int num = (kernel.length-1)/2;
		
		int[][] temp = new int[h][w];
		for(int i=1; i<h-1; i++){
			for(int j=1; j<w-1; j++){
				double red= 0, green = 0, blue = 0;
				for(int a = -num; a <= num; a++){
					for( int b= -num; b <= num; b++){
						red += this.howRed(pix2[i+a][j+b])      *(kernel[a+num][b+num]);
						green += this.howGreen(pix2[i+a][j+b])  *(kernel[a+num][b+num]);
						blue += this.howBlue(pix2[i+a][j+b])    *(kernel[a+num][b+num]);
					}
				}
				temp[i][j] = combine(0,(int)red,(int)green,(int)blue);
			}
		}
//		for(int i=1; i<h-1; i++){
//			for(int j=1; j<w-1; j++){
//				int red=0, green=0, blue=0;
//				for( int a=-((kernel.length-1)/2); a<kernel.length/2; a++){
//					for( int b=-kernel.length/2; b<kernel.length/2; b++){
//						red += this.howRed(pix2[i+a][j+b])        *(pix[a+kernel.length/2][b+kernel.length/2]);
//						green += this.howGreen(pix2[i+a][j+b])    *(pix[a+kernel.length/2][b+kernel.length/2]);
//						blue += this.howBlue(pix2[i+a][j+b])      *(pix[a+kernel.length/2][b+kernel.length/2]);
//						
//					}
//				}
//				temp[i][j] = combine(0,red,green,blue);
//			}
//		}
		return temp;
	}
	public void emboss(int direction, int strength, int size){
		String[] temp = {"top", "bottom", "left", "right", "top-left", "top-right", "bottom-left", "bottom-right"};
		String dir = (String) JOptionPane.showInputDialog(this,"Pick a Direction for the light. From the...","Emboss",JOptionPane.PLAIN_MESSAGE,null,temp,temp[temp.length-1]);
		if(dir == null){
			return;
		}
		strength = Integer.parseInt(JOptionPane.showInputDialog(this, "Strength?"));
		if(dir == null){
			return;
		}
		this.blackandwhite();
		if(dir.equals("top")){
		pix = this.applyKernel(pix, new int[][] 
		      {{-strength, -strength, -strength}, 
				{0,  0,  0},
				{strength,  strength,  strength}});
		}
		if(dir.equals( "bottom")){
			pix = this.applyKernel(pix,new int[][] 
					 {{strength,  strength,  strength},
					 {0,              0,  0},
					{-strength, -strength, -strength}});
			}
		if(dir.equals( "left")){
			pix = this.applyKernel(pix,new int[][] 
					{{-strength,  0,  strength},
					{-strength,  0,  strength},
					{-strength,  0,  strength}});
			}
		if(dir.equals( "right")){
			pix = this.applyKernel(pix,new int[][] 
					 {{strength,  0, -strength},
					 {strength,  0, -strength},
					 {strength,  0, -strength}});
			}
		if(dir.equals( "top-left")){
			pix = this.applyKernel(pix,new int[][] 
					{{-strength, -strength,  0},
					{-strength,  0,  strength},
					 {0,  strength,  strength}});
			}
		if(dir.equals( "top-right")){
			pix = this.applyKernel(pix,new int[][] 
					 {{0, -strength, -strength},
					 {strength,  0, -strength},
					 {strength,  strength,  0}});
			}
		if(dir.equals( "bottom-left")){
			pix = this.applyKernel(pix,new int[][] 
					 {{0,  strength,  strength},
					{-strength,  0,  strength},
					{-strength, -strength,  0}});
			}
		if(dir.equals( "bottom-right")){
			pix = this.applyKernel(pix,new int[][] 
					 {{strength,  strength,  0},
					 {strength,  0, -strength},
					 {0, -strength, -strength}});
			}
		for(int i=1; i<h-1; i++){
			for(int j=1; j<w-1; j++){
				double red = this.howRed(pix[i][j])+ 128;
				double green = this.howGreen(pix[i][j])+ 128;
				double blue = this.howBlue(pix[i][j])+ 128;
				pix [i][j] = this.combine(0,(int)red,(int)green,(int)blue);
			}
		}
		this.arrayToImg(pix);
	}
	//0 = UD; 1 = LR; 2 = both
	public void edgeDectect(int dir){
		int strength = Integer.parseInt(JOptionPane.showInputDialog(this,"Strength?"));
		int[][] vert1 = new int[1][1], vert2 = new int[1][1], horiz1 = new int[1][1], horiz2 = new int[1][1];
		if(dir != 0){
			vert1 = this.applyKernel(pix,new int[][] 
		                				    {{strength,  0, -strength},
		                					 {strength,  0, -strength},
		                					 {strength,  0, -strength}});
			vert2 = this.applyKernel(pix,new int[][] 
		   		               			    {{-strength,  0, strength},
		   		               				 {-strength,  0, strength},
		   		               				 {-strength,  0, strength}});
			if(dir == 1){
				pix = this.combine(vert1,vert2);
			}
		}
		if(dir != 1){
			horiz1 = this.applyKernel(pix,new int[][] 
		   		               			    {{-strength,  -strength, -strength},
		   		               				 {        0,          0,         0},
		   		                     		 {strength,   strength,   strength}});
			horiz2 = this.applyKernel(pix,new int[][] 
			   		               			{{ strength,  strength,   strength},
			   		                    	 {       0,         0,         0},
			   		              		   	 {-strength,  -strength, -strength}});
			if(dir == 0){
				pix = this.combine(horiz1,horiz2);
			}
		}
		if(dir == 2){
			pix = this.combine(this.combine(horiz1,horiz2),this.combine(vert1,vert2));
		}
		
		this.arrayToImg(pix);
	}
	public void contrast(int cby){
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				int red, green, blue;
				if(this.howRed(pix[i][j])>128)
					red = this.howRed(pix[i][j]) + cby;
				else if(this.howRed(pix[i][j])<128)
					red = this.howRed(pix[i][j]) - cby;
				else 
					red = 128;
				red = Math.max(0,Math.min(red,255));
				if(this.howGreen(pix[i][j])>128)
					green = this.howGreen(pix[i][j]) + cby;
				else if(this.howGreen(pix[i][j])<128)
					green = this.howGreen(pix[i][j]) - cby;
				else 
					green = 128;
				green = Math.max(0,Math.min(green,255));
				if(this.howBlue(pix[i][j])>128)
					blue = this.howBlue(pix[i][j]) + cby;
				else if(this.howBlue(pix[i][j])<128)
					blue = this.howBlue(pix[i][j]) - cby;
				else 
					blue = 128;
				blue = Math.max(0,Math.min(blue,255));
				pix[i][j] = this.combine(0,red,green,blue);
			}
		}
		this.arrayToImg(pix);
	}
	/* **************MATRIX STUFF ********************************* */
	public void timesMatrix(double num1, double num2, double num3, double num4){
		int[][] pix = imgToArray();
		double maxX = 0, maxY = 0, minX = Double.MAX_VALUE, minY = Double.MAX_VALUE ;
		//int[][] temp = new int[3][3];
		for(int y=0; y<h; y++){
			for(int x=0; x<w; x++){
				int newX = (int) (num1*x + num2*y);
				int newY =(int) (num3*x + num4*y); 
				if(newX<minX){
					minX = newX;
				}
				if(newX>maxX){
					maxX = newX;
				}
				if(newY<minY){
					minY = newY;
				}
				if(newY>maxY){
					maxY = newY;
				}
			}
		}
		int[][] temp = new int[(int)Math.round( maxY-minY+1)][(int)Math.round( maxX-minX+1)];
		for(int y=0; y<temp.length; y++){
			for(int x=0; x<temp[0].length; x++){
				temp[y][x] = 0xFF7F7F7F;
			}
		}
		double[][] mat = {{num1, num2, -1*minX},{num3,num4,-1*minY},{0,0,1}};
		Matrix forward = new Matrix(mat);
		Matrix back = forward.inverse();
		for(int r=0; r<temp.length; r++)
			for(int c=0; c<temp[0].length; c++){
				double[] newXY = {c,r};
				double[] oldXY = back.mult(newXY);
				try{
					temp[ (int)Math.round(newXY[1]) ][ (int)Math.round(newXY[0]) ] = pix[ (int)Math.round(oldXY[1]) ][ (int)Math.round(oldXY[0]) ];
				}catch(Exception ex){/*this is a "dead spot" outside the original image*/}
			}
				
	
		this.arrayToImg(temp);
	}
	public void resize(double ratio){
		timesMatrix(ratio,0,0,ratio);
	}
	public void stretch(boolean vert, double ratio){
		if(vert){
			timesMatrix(ratio,0,0,1);
		}
		if(!vert){
			timesMatrix(1,0,0,ratio);
		}
	}
	public void rotate(double angle){
		this.timesMatrix(Math.cos(angle),-Math.sin(angle),Math.sin(angle),Math.cos(angle));
	}
	public void paint(Graphics g){
		super.paint(g);
		((Graphics2D)g).drawImage(img,null,0,0);
		g = img.getGraphics();
		g.setColor(ImageEditor.markerColor);
		//Image buffer = createImage(this.getWidth(),this.getHeight());
		//Graphics b = buffer.getGraphics();
		
		if(m2x!=0 && m2y!=0){
			g.drawLine(m1x,m1y,m2x,m2y);
		}
		pix = this.imgToArray();
		//g.drawImage(buffer,0,0,this);
		this.repaint();
	}
/**  **************** START WITH THESE!  ************** **/
	//have kids do this first!  just take the pixels and replace them
	public void tester(){
		int[][] blah = imgToArray();		
		arrayToImg(blah);
		//this.red();
		
	}

	//Postconditions:  all of the pixels from the original image have been stored
	//  into a 2d array and that 2d array has been returned
	private int[][] imgToArray(){
		//this puts the pixels into a 1d array.  You want to move them into a 2d array
		int[] pix = img.getRGB(0, 0, w, h, null, 0, w);
		
		int[][] ans = new int[h][w];
		
		int count = 0;
		
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				ans[i][j] = pix[count++];
			}
		}
		
		return ans;
	}
	public void undo(){
		redo.push(img);
		if(undo.size()!=0)
			img = undo.pop();
	}
	public void redo(){
		
		if(undo.size()!=0)
			img = redo.pop();
	}
	
	//Postconditions:  the pixel values from the given 2d array have been loaded onto
	//  the image
	//HINT:  use this function--> img.setRGB(x,y,val);
	private void arrayToImg(int[][] pix){
		h = pix.length;
		w = pix[0].length;
		
		//undo.push(pix);
		
		undo.push(img);
		
		this.setPreferredSize(new Dimension(w,h));
		img = new BufferedImage(w,h,img.getType());
		
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				img.setRGB(j,i,pix[i][j]);
			}
		}
		
		
		
		this.repaint();
	}
	public void saveFile(File f){
		  try{ImageIO.write(img,"jpg",f);}catch(IOException e){JOptionPane.showMessageDialog(null, e.getMessage());}

		}
/**  ***************************************************  **/
	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {
		m2x = 0;
		m2y = 0;
		m1x = 0;
		m1y = 0;
	}
	public void mouseDragged(MouseEvent e) {
		m2x=m1x;
		m2y=m1y;
		m1x=e.getX();
		m1y=e.getY();
	}
	public void mouseMoved(MouseEvent arg0) {}
}
