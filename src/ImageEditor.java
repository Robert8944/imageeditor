import java.awt.*;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.io.*;
import java.awt.event.*;

public class ImageEditor extends JFrame implements ActionListener, ChangeListener, WindowListener{
	
	private static final long serialVersionUID = 1L;
	private ImageCanvas orig, alter;
	private int lbby, lcby;
	//private JTextField resizeNum, bitsNum;
	private JMenuItem blur, rotate, sharpen, returnToNormal, negative, mirrorUD, mirrorLR,
		resize, toBit, timesKernel, undo, redo, stretchUD, stretchLR, emboss, baw, sepia,
		verticalE, horizontalE, bothE, chooseColor;
	private JScrollPane alterS, origS;
	JButton done = new JButton("This One!");
	JTextField TL, TR, BL, BR;
	JDialog kernel;
	JPanel setUp;
	public static Color markerColor;
	private JPanel setUp2;
	private JSlider brightness, contrast;
	private String file = "flower.jpg";
	private boolean hasBeenSaved = false;
	private File mrAlterSave = new File(file);
	private JMenuItem open, save, saveAs, print,Normal,Red,Green,Blue,Yellow,Magenta,Cyan;
	//private Timer t;
	
	public ImageEditor(){
		super("Image Editor");
		
		kernel = new JDialog(this, "Kernel", true, null);
		//kernel.setPositionRelitiveTo(null));
		kernel.setDefaultCloseOperation(HIDE_ON_CLOSE);
		done.addActionListener(this);
		TL = new JTextField("");
		TR = new JTextField("");
		BL = new JTextField("");
		BR = new JTextField("");
		setUp = new JPanel();
		setUp.setLayout(new GridLayout(2,2));
		setUp.add(TL);
		setUp.add(TR);
		setUp.add(BL);
		setUp.add(BR);
		kernel.add(setUp,BorderLayout.CENTER);
		kernel.add(done,BorderLayout.SOUTH);
		kernel.setSize(100,100);
		kernel.setLocationRelativeTo(null);
		//JOptionPane.showMessageDialog(this,"☺♥☻");
		
		ImageIcon imgIcon = new ImageIcon("photoshop_icon.jpg");
		Image img = imgIcon.getImage();
		this.setIconImage(img);
		
		markerColor = Color.BLACK;
		
		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());} catch (Exception e) {}
		
		orig = new ImageCanvas();
		orig.setImage(new File(file));
		
		alter = new ImageCanvas();
		alter.setImage(new File(file));
		
		alter.tester();
		
		negative = new JMenuItem("Negative");
		negative.addActionListener(this);
		mirrorUD = new JMenuItem("MirrorUD");
		mirrorUD.addActionListener(this);
		mirrorLR = new JMenuItem("MirrorLR");
		mirrorLR.addActionListener(this);
		blur = new JMenuItem("Blur");
		blur.addActionListener(this);
		sharpen = new JMenuItem("Sharpen");
		sharpen.addActionListener(this);
		returnToNormal = new JMenuItem("Reset");
		returnToNormal.addActionListener(this);
		resize = new JMenuItem("Resize");
		resize.addActionListener(this);
		toBit = new JMenuItem("to bit colors");
		toBit.addActionListener(this);
		timesKernel = new JMenuItem("Multiply by a kernel");
		timesKernel.addActionListener(this);
		rotate = new JMenuItem("Rotate");
		rotate.addActionListener(this);
		undo = new JMenuItem("undo");
		undo.addActionListener(this);
		redo = new JMenuItem("redo");
		redo.addActionListener(this);
		stretchLR = new JMenuItem("stretchLR");
		stretchLR.addActionListener(this);
		stretchUD = new JMenuItem("stretchUD");
		stretchUD.addActionListener(this);
		emboss = new JMenuItem("emboss");
		emboss.addActionListener(this);
		baw = new JMenuItem("Black and White");
		baw.addActionListener(this);
		sepia = new JMenuItem("Sepia Tone");
		sepia.addActionListener(this);
		verticalE = new JMenuItem("vertical edges");
		verticalE.addActionListener(this);
		horizontalE = new JMenuItem("horizontal edges");
		horizontalE.addActionListener(this);
		bothE = new JMenuItem("both edges");
		bothE.addActionListener(this);
		print = new JMenuItem("Print...");
		print.addActionListener(this);
		chooseColor = new JMenuItem("Change Marker Color");
		chooseColor.addActionListener(this);
		
		setUp2 = new JPanel();
		setUp2.setLayout(new GridLayout(1,2));
		
		contrast = new JSlider(-100,100);
		contrast.addChangeListener(this);
		contrast.setMajorTickSpacing(10);
		contrast.setMinorTickSpacing(2);
		contrast.setPaintTicks(true);
		contrast.setPaintLabels(true);
		//contrast.setSnapToTicks(true);
		contrast.setBorder(BorderFactory.createTitledBorder("Contrast"));
		contrast.setFocusable(false);
		setUp2.add(contrast);
		
		brightness = new JSlider(-255,255);
		brightness.addChangeListener(this);
		brightness.setMajorTickSpacing(20);
		brightness.setMinorTickSpacing(4);
		brightness.setPaintTicks(true);
		brightness.setPaintLabels(true);
		//brightness.setSnapToTicks(true);
		brightness.setBorder(BorderFactory.createTitledBorder("brightness"));
		brightness.setFocusable(false);
		setUp2.add(brightness);
		//brightness.setOrientation(0);
		
		//t = new Timer(1, this);
		
		makeMenu();
		this.add(setUp2,BorderLayout.NORTH);
		
		this.addWindowListener(this);
		
		JPanel stuff = new JPanel();
		stuff.setLayout(new GridLayout(1,2));
		origS = new JScrollPane(orig);
		stuff.add(origS);
		alterS= new JScrollPane(alter);
		stuff.add(alterS);
		this.add(stuff, BorderLayout.CENTER);
		//finishing up
		this.setSize(900,700);
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setVisible(true);
		
	}
	
	private void makeMenu(){
		JMenuBar bar = new JMenuBar();
		JMenu file = new JMenu("File");
		open = new JMenuItem("Open...");
		open.addActionListener(this);
		file.add(open);
		save = new JMenuItem("Save");
		save.addActionListener(this);
		file.add(save);
		saveAs = new JMenuItem("Save As...");
		saveAs.addActionListener(this);
		file.add(saveAs);
		file.add(print);
		bar.add(file);
		
		JMenu edit = new JMenu("Edit");
		edit.add(undo);
		edit.add(redo);
		edit.add(returnToNormal);
		bar.add(edit);

		JMenu colorm = new JMenu("Color Stuff");
		JMenu colorms = new JMenu("Shades");
		Normal = new JMenuItem("Normal");
		Normal.addActionListener(this);
		Red = new JMenuItem("Red");
		Red.addActionListener(this);
		Green = new JMenuItem("Green");
		Green.addActionListener(this);
		Blue = new JMenuItem("Blue");
		Blue.addActionListener(this);
		Yellow = new JMenuItem("Yellow");
		Yellow.addActionListener(this);
		Magenta = new JMenuItem("Magenta");
		Magenta.addActionListener(this);
		Cyan = new JMenuItem("Cyan");
		Cyan.addActionListener(this);
		colorms.add(Normal);
		colorms.add(Red);
		colorms.add(Green);
		colorms.add(Blue);
		colorms.add(Yellow);
		colorms.add(Magenta);
		colorms.add(Cyan);
		colorm.add(colorms);
		colorm.add(negative);
		colorm.add(baw);
		colorm.add(sepia);
		colorm.add(chooseColor);
		bar.add(colorm);
		
		JMenu kernelS = new JMenu("Kernel Stuff");
		JMenu edgesm = new JMenu("Edges");
		edgesm.add(verticalE);
		edgesm.add(horizontalE);
		edgesm.add(bothE);
		kernelS.add(edgesm);
		kernelS.add(blur);
		kernelS.add(sharpen);
		kernelS.add(emboss);
		kernelS.add(resize);
		kernelS.add(rotate);
		kernelS.add(stretchLR);
		kernelS.add(stretchUD);
		kernelS.add(timesKernel);
		bar.add(kernelS);
		
		JMenu random = new JMenu("Random");
		random.add(mirrorUD);
		random.add(mirrorLR);
		random.add(toBit);
		bar.add(random);
		
		this.setJMenuBar(bar);
	}

	public void pickColor(){
		markerColor = JColorChooser.showDialog(this,"ChooseColor",markerColor);
	}
	
	public void PrintImage() {
		Object[] options = {"Save then print","print last save","cancel print"};
		int n = JOptionPane.showOptionDialog(this,"you need to save your alterations to print them, doy you want to save?","Print...",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
		if(n==0){
			this.actionPerformed(new ActionEvent(saveAs, 0, null));
		}
		else if(n==2){return;}
		else if(n==1){

			PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
			pras.add(new Copies(Integer.parseInt(JOptionPane.showInputDialog(this,"How many copies?"))));
			PrintService pss[] = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);
			if (pss.length == 0)
				JOptionPane.showMessageDialog(this,"No printer services available.","No pirnters",JOptionPane.ERROR_MESSAGE);
			PrintService ps = pss[0];
			ps = (PrintService) JOptionPane.showInputDialog(this,"Select a Printer","Print...",JOptionPane.PLAIN_MESSAGE,null,pss,pss[0].toString());
			if(ps == null){
				return;
			}
			DocPrintJob job = ps.createPrintJob();
			try{
				FileInputStream fin = new FileInputStream(mrAlterSave);
				Doc doc = new SimpleDoc(fin, DocFlavor.INPUT_STREAM.GIF, null);
				job.print(doc, pras);
				fin.close();
			}catch(Exception ex){}
		}
	}
	
	public static void main(String[] args) {new ImageEditor();}

	
	public void actionPerformed(ActionEvent e) {
		
		
		if(e.getSource()==open){
			JFileChooser jfc = new JFileChooser();
			int result = jfc.showOpenDialog(this);
			if(result == JFileChooser.CANCEL_OPTION)
				return;
			File f = jfc.getSelectedFile();
			orig.setImage(f);
			alter.setImage(f);
			file = f.getAbsolutePath();
			this.repaint();
		}
		if(e.getSource()==saveAs){
			hasBeenSaved = true;
			MyFileFilter filter = new MyFileFilter("jpg");
			File savedFile;
			JFileChooser fileChooser = new JFileChooser("\\");
			fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			fileChooser.setFileFilter(filter);

			int result = fileChooser.showSaveDialog(getParent());

			if(result != JFileChooser.CANCEL_OPTION){
				savedFile=fileChooser.getSelectedFile();
				if(fileChooser.accept(savedFile)){
					alter.saveFile(savedFile);
				}
				else
					JOptionPane.showMessageDialog(null, "Must save to .jpg file");
			}
			else{
				savedFile=null;
			}
			if(savedFile!=null){
				mrAlterSave = savedFile;
			}

		}
		if(e.getSource()==save){
			if(hasBeenSaved)
				alter.saveFile(mrAlterSave);
			else
				this.actionPerformed(new ActionEvent(saveAs, 0, null));
		}
		
		if(e.getSource()==Normal)
			alter.normal();
		if(e.getSource()==Red)
			alter.red();
		if(e.getSource()==Green)
			alter.green();
		if(e.getSource()==Blue)
			alter.blue();
		if(e.getSource()==Cyan){
			alter.negative();
			alter.red();
			alter.negative();
			alter.red();
			alter.negative();
		}
		if(e.getSource()==Magenta){
			alter.negative();
			alter.green();
			alter.negative();
			alter.green();
			alter.negative();
		}
		if(e.getSource()==Yellow){
			alter.negative();
			alter.blue();
			alter.negative();
			alter.blue();
			alter.negative();
		}
		if(e.getSource()==negative){
			alter.negative();
			this.repaint();
		}
		if(e.getSource()==blur){
			alter.blur();
			this.repaint();
		}
		if(e.getSource()==sharpen){
			alter.sharpen();
			this.repaint();
		}
		if(e.getSource()==returnToNormal){
			//t.stop();
			brightness.setValue(0);
			contrast.setValue(0);
			alter.setImage(new File(file));
			this.repaint();
		}
		if(e.getSource()==mirrorLR){
			alter.mirror(true);
			this.repaint();
		}
		if(e.getSource()==mirrorUD){
			alter.mirror(false);
			this.repaint();
		}
		if(e.getSource()==timesKernel){
		
		kernel.setVisible(true);
		this.repaint();
		}
		if(e.getSource()==chooseColor){
			
			this.pickColor();
		}
		//if(e.getSource()==){
			//if(!t.isRunning())
				//t.start();
			//else{
				//t.stop();
			//}
		
		//}
		if(e.getSource()==done){
			kernel.setVisible(false);
			double x1=0, x2=0, x3=0, x4=0;
			try{
				x1 = Double.parseDouble(TL.getText());
			}catch(Exception ex){
				if(TL.getText().equals("")){
					x1=0;
				}
				else if(TL.getText().substring(0,1).equals("c")){
					x1=Math.cos(Double.parseDouble(TL.getText().substring(5,(TL.getText().length()-1))));
				}
				
				else return;
			}
			try{
				x2 = Double.parseDouble(TR.getText());
			}catch(Exception ex){
				if(TL.getText().equals("")){
					x2=0;
				}
				else return;
			}
			try{
				x3 = Double.parseDouble(BL.getText());
			}catch(Exception ex){
				if(TL.getText().equals("")){
					x3=0;
				}
				else return;
			}
			try{
				x4 = Double.parseDouble(BR.getText());
			}catch(Exception ex){
				if(TL.getText().equals("")){
					x4=0;
				}
				else return;
			}
//			kernel.setVisible(false);
//			kernel.dispose();
			alter.timesMatrix(x1,x2,x3,x4);
		}
		if(e.getSource()==resize){
			alter.resize(Double.parseDouble(JOptionPane.showInputDialog(this,"Ratio?")));
		}
		if(e.getSource()==rotate){
			alter.rotate((Double.parseDouble(JOptionPane.showInputDialog(this,"Degrees?"))*Math.PI)/180);
		}
		if(e.getSource()==toBit){
			String[] options = {"1","2","4","8","16"};
			alter.toBits(Integer.parseInt((String) JOptionPane.showInputDialog(this,"Select a number of bits","Number of bits?",JOptionPane.PLAIN_MESSAGE,null,options,"1")));
		}
		if(e.getSource()==undo){
			alter.undo();
		}
		if(e.getSource()==redo){
			alter.redo();
		}
		if(e.getSource()==stretchLR){
			alter.stretch(true,Double.parseDouble(JOptionPane.showInputDialog(this,"Ratio?")));
		}
		if(e.getSource()==stretchUD){
			alter.stretch(false,Double.parseDouble(JOptionPane.showInputDialog(this,"Ratio?")));
		}
		if(e.getSource()==emboss){
			alter.emboss(1,1,1);
		}
		if(e.getSource()==baw){
			alter.blackandwhite();
		}
		if(e.getSource()==sepia){
			alter.sepia(Double.parseDouble(JOptionPane.showInputDialog(this,"Speia Depth?(20 reccomended)")),Double.parseDouble(JOptionPane.showInputDialog(this,"Speia Intensity?(20 reccomended)")));
		}
		if(e.getSource()==print){
			this.PrintImage();
		}
		//verticalE, horizontalE, bothE
		if(e.getSource()==verticalE){
			alter.edgeDectect(1);
		}
		if(e.getSource()==horizontalE){
			alter.edgeDectect(0);
		}
		if(e.getSource()==bothE){
			alter.edgeDectect(2);
		}
		alterS.updateUI();
		origS.updateUI();
		//if(e.getSource()==t){
		//}
	}

	public void stateChanged(ChangeEvent arg0) {
	
		if(arg0.getSource() == brightness){
			
			alter.brighten(brightness.getValue() - lbby);
			lbby = brightness.getValue();
		}
		if(arg0.getSource() == contrast){
			
			alter.contrast((int) Math.round((contrast.getValue() - lcby)*1.28));
			lcby = contrast.getValue();
		}
		
	}


	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {
		Object[] options = {"Save and quit",
				"Quit",
				"Cancel"};
			int n = JOptionPane.showOptionDialog(this,
					"Would you like to save changes?",
					"Save?",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[1]);
			if(n == 0){
				this.actionPerformed(new ActionEvent(save,0,null));
				System.exit(0);
			}
			else if(n == 1){
				System.exit(0);
			}
			else if(n == 2){
			}
}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
}
