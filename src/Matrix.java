
public class Matrix {
	private double[][] m;
	
	
	public Matrix(int[] r0, int[] r1){
		m = new double[3][3];
		for(int c=0; c<3; c++)
			m[0][c] = r0[c];
		for(int c=0; c<3; c++)
			m[1][c] = r1[c];
		m[2][2] = 1;
	}
	
	public Matrix(double[][] data){
		m = data;
	}	
	
	public Matrix inverse(){
		double[][] inv = new double[3][3];
		double det = m[0][0]*(m[1][1]*m[2][2] - m[1][2]*m[2][1]) - m[1][0]*(m[0][1]*m[2][2] - m[0][2]*m[2][1]) + m[2][0]*(m[0][1]*m[1][2] - m[0][2]*m[1][1]);
		
		inv[0][0] = 1/det*( m[2][2]*m[1][1] - m[2][1]*m[1][2] );
		inv[0][1] = -1/det*( m[2][2]*m[0][1] - m[2][1]*m[0][2] );
		inv[0][2] = 1/det*( m[1][2]*m[0][1] - m[1][1]*m[0][2] );
		
		inv[1][0] = -1/det*( m[2][2]*m[1][0] - m[2][0]*m[1][2] );
		inv[1][1] = 1/det*( m[2][2]*m[0][0] - m[2][0]*m[0][2] );
		inv[1][2] = -1/det*( m[1][2]*m[0][0] - m[1][0]*m[0][2] );

		inv[2][0] = 1/det*( m[2][1]*m[1][0] - m[2][0]*m[1][1] );
		inv[2][1] = -1/det*( m[2][1]*m[0][0] - m[2][0]*m[0][1] );
		inv[2][2] = 1/det*( m[1][1]*m[0][0] - m[1][0]*m[0][1] );
		
		return new Matrix(inv);
	}
	
	public double[] mult( double[] xy ){
		double[] ans = new double[2];
		ans[0] = m[0][0]*xy[0] + m[0][1]*xy[1] + m[0][2];
		ans[1] = m[1][0]*xy[0] + m[1][1]*xy[1] + m[1][2];
		return ans;
	}
	
	public Matrix mult( Matrix m2){
		double[][] prod = new double[3][3];
		
		for(int r=0; r<3; r++){
			for(int c=0; c<3; c++){
				double spot=0;
				for(int x=0; x<3; x++){
					spot+=m[r][x] * m2.m[x][c];
				}
				prod[r][c] = spot;
			}
		}
		return new Matrix(prod);
	}
	
	public String toString(){
		String s = "";
		for(int r=0; r<3; r++){
			for(int c=0; c<3; c++)
				s+=m[r][c]+" ";
			s+="\n";
		}
		return s;
	}
	
	public static void main(String[] args){
		//int[] r0 ={1,2,3};
		//int[] r1 = {4,5,6};
		//double[][] a = {{1,2,3},{4,5,6},{7,8,9}};
		double[][] b = {{3,1,2},{6,5,6},{9,8,7}};
		
		Matrix m1 = new Matrix(b);
		Matrix m2 = m1.inverse();
		System.out.println(m2);
		System.out.println(m1.mult(m2));
		
	}
	
}
